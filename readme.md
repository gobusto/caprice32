CaPriCe32 - Amstrad CPC Emulator
================================

What is it?
-----------

Caprice32 is a software emulator of the Amstrad CPC 8bit home computer
series. The emulator faithfully imitates the CPC464, CPC664, and CPC6128
models. By recreating the operations of all hardware components at a low
level, the emulator achieves a high degree of compatibility with original
CPC software. These programs or games can be run unmodified at real-time
or higher speeds, depending on the emulator host environment.

Requirements
------------

You will need the following to successfully compile an executable:

+ MinGW (only for Windows) - <http://sourceforge.net/projects/mingw/>
+ SDL - <http://www.libsdl.org/>
+ zLib - <http://www.gzip.org/zlib/>

Compiling
---------

### Windows

Edit the makefile to update the IPATHS directive with the correct location
of the SDL include and library files.

Use `make` (or `mingw32-make`, depending on how you have your MinGW installed)
to compile a developer build, or use `make RELEASE=TRUE` for an optimized
executable.

### Linux

use `make -f makefile.unix` to compile a developer build, or use
`make -f makefile.unix RELEASE=TRUE` for an optimized executable.

Keyboard
--------

The following values are taken from the US keyboard definitions in `cap32.cpp`:

+ PAUSE         BREAK
+ SCREENSHOT    PRINT
+ FULLSCREEN    F1
+ LOAD SNAPSHOT F2
+ LOAD TAPE     F3
+ SAVE SNAPSHOT F4
+ RESET         F5
+ LOAD DRIVE A  F6
+ LOAD DRIVE B  F7
+ OPTIONS       F8
+ EXIT          F10
+ MF2 STOP      F11
+ SPEED         F12

Some keys have other effects if CTRL is held down:

+ PLAY TAPE     F3
+ MF2 RESET     F5
+ JOYSTICK      F8
+ FPS           F12

COMMENTS OR READY TO CONTRIBUTE?
--------------------------------

If you have suggestions, a bug report or even want to join the development team,
please feel free to use one of the many contact methods presented on the
Caprice32 project page on SourceForge.net:

<http://sourceforge.net/projects/caprice32/>

LICENSE
-------

(c) Copyright 1997-2004 Ulrich Doewich

The source for Caprice32 is distributed under the terms of the GNU General
Public License (GNU GPL), which is included in this archive as `gpl.txt`.
Please make sure that you understand the terms and conditions of the license
before using the source.

I encourage you to get involved in the project - please see the Caprice32
pages on SourceForge.net for contact details.
